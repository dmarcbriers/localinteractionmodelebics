from __future__ import division
import networkx as nx
import Analysis as af
from random import *
import math as math
import numpy as np
from scipy import spatial

# This class holds slgorithms for geenrating patterns from a preloaded
# network structure. A state variable is used to define what the state of
# the node at that location is. In geenral these are designed to work
# with the simulation object StmeCell which already has a state field,
# but really any object which has the state predfeind to U can be used

def Zero(network):
    """ Resets a network to have no differentiated cells. Use this 
        when using the structure of neworks taken from a simulation """
    nodes = network.nodes()
    #print len(nodes)
    for i in range(0,len(nodes)):
        nodes[i].state = "U"
    #print len(nodes) 
    return network


def Random(percent, network):
    '''Generates a random pattern from the given loaded network'''
    #Set up vars
    #n_percent = 0.0
    #get the nodes
    nodes = network.nodes()
    
    indices = range(0,len(nodes))
    num_nodes = math.ceil(percent*len(nodes))
    rand_indices = np.random.choice(indices,size=num_nodes,replace=False)
    
    #Loop
    for i in rand_indices:
        nodes[i].state = "D"
    
    """while(n_percent < percent):
        #pick a random node and change it
        index = randint(0, count-1)
        nodes[index].state = "D"
        #count all fo the ndoes with a state of D
        d_count = 0
        for i in range(0, len(nodes)):
            if(nodes[i].state == "D"):
                d_count += 1
        #get the number of nodes with this state
        n_percent = 100.0*float(d_count)/float(count)
    """
    return network

def OutsideIn(percent_radius, network):
    '''Generates an outside in pattern with cutoff of percent radius'''
    #cent = af.GetCenter(network)
    radius,center = af.GetRadiusandCenter(network)
    threshold = percent_radius*radius
    
    for n in network.nodes():
        #check if the node is close to the center
        p = n.location
        x = p[0] - center[0]
        y = p[1] - center[1]
        z = p[2] - center[2]
        #get the dist
        mag = math.sqrt(x*x + y*y + z*z)
        if(mag >= threshold):
            #change it
            n.state = "D"
    return network

def InsideOut(percent_radius, network):
    '''Generates an inside out pattern with cutoff of radius'''
    radius,center = af.GetRadiusandCenter(network)
    threshold = percent_radius*radius
    #cent = af.GetCenter(network)
    for n in network.nodes():
        #check if the node is close to the center
        p = n.location
        x = p[0] - center[0]
        y = p[1] - center[1]
        z = p[2] - center[2]
        #get the dist
        mag = math.sqrt(x*x + y*y + z*z)
        if(mag <= threshold):
            #change it
            n.state = "D"
    return network

def Globular(seeds, count, network):
    '''Generates globular patterns controlled by the number of
        initial seeds and the size of each resulting glob'''
    for i in range(0, seeds):
        #Pick a random node
        nodes = network.nodes()
        index = randint(0, len(nodes)-1)
        n = nodes[index]
        #set this nodes state
        n.state = "D"
        #get all the neighboring nodes
        cons = network.neighbors(n)
        #sort through all of this
        for c in cons:
            _Globular(c, 1, count, network)
    return network
            
def _Globular(node, count, max_count, network):
    #make sure that count is less than maxcount
    st = node.state
    if(max_count > count and st == "U"):
        #set this nodes state
        node.state = "D"
        #get all the neighboring nodes
        cons = network.neighbors(node)
        #sort through all of this
        for c in cons:
            _Globular(c, count + 1, max_count, network)
            
    
        
def Snaked(seeds, length, network):
    '''Generates snake-like patterns controlled by the number of
        initial seeds a dn the length of each resulting pattern'''
    for i in range(0, seeds):
        #Pick a random node
        nodes = network.nodes()
        index = randint(0, len(nodes)-1)
        n = nodes[index]
        #set this nodes state
        n.state = "D"
        #get all the neighboring nodes
        cons = network.neighbors(n)
        if(len(cons) != 0):  
            #pick one of these randomly
            index = randint(0, len(cons)-1)
            #then recurse on it
            _Snaked(cons[index], 1, length, network)
    return network
        

def _Snaked(node, count, max_count, network):
    #make sure that count is less than maxcount
    if(max_count > count):
        #set this nodes state
        node.state = "D"
        #get all the neighboring nodes
        cons = network.neighbors(node)
        if(len(cons) != 0):
            #pick one of these randomly
            index = randint(0, len(cons)-1)
            #then recurse on it
            _Snaked(cons[index], count + 1, max_count, network)
    #otherwise do nothing

def cluster_distance(objectA, ObjectB,max_distance=None):
    """ 
    Custom function to compute the distance between stem cell objects
    in a Simulaiton.
    
    Metric:
    ---------------
        1 if euclidean distance <= max_distance
    """
    if max_distance == None:
        max_distance = objectA.radius + objectB.radius

    if spatial.distance.euclidean(objectA.location,objectB.location) <= max_distance \
        and objectA.state == objectB.state:
        distance = 0
    else:
        distance = 1
    
    return distance

"""
cluster_labels = DBSCAN(
            eps=500,
            min_samples=max(2, len(found_geopoints)/10),
            metric=geodistance
).fit(np.array(found_geopoints)).labels_

"""
