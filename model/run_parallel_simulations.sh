## This script is a example of running simulations in parallel on a SIGNLE node.
## You can have one job per node if you would like.

SECONDS=0

##Run this from the same dir as ModelSetup.py
echo {0..3} | tr ' ' '\n' | parallel --gnu --jobs 4 'python ModelSetup.py {} .01 0.1 25 0.9 25 > sim{}.log'

run_minutes=$(expr $SECONDS / 60)
jobcomplete "Finished running parallel jobs on hyness box in $run_minutes minutes."

