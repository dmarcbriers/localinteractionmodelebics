from Simulation import *
#from PIL import ImageGrab, Image
import networkx as nx
#from visual import *
import time as t_m
import subprocess
import os,sys
import math
import PatternGenerator as PG
import Analysis as af
import numpy as np

def count_states(agents,time):
    
    total=len(agents)
    D=0
    U=0

    for agent in agents:
        if (agent.state == "D"):
            D +=1
        if(agent.state == "T"):
            D+=1
        if (agent.state == "U"):
            U +=1
    
    #Total number of cells
    tot = U + D

    # Percent Differentiated    
    if D > 0:
        percent_diff = float(D)/float(tot)*100
    else:
    	percent_diff = 0

    #print ("Time\tCells\tPDiff\tU\tD\n%i\t%i\t%.03f\t%i\t%i") % (time,tot,percent_diff,U,D)
    #print time image_name total_cells pdiff Udiff Difff 
    
    return percent_diff

#path1 = "/home/dbriers/hyness/EBICS/clean_model/results"
#paths = [path1]

#Load Data
#folder = 'E:\\BU\\Belta Rotation\\EBICS\\localinteractionmodelebics\\model\\results_0.01_0.3_25.0_0.5_25.0'
folder = "/home/dbriers/hyness/EBICS/portable_interaction_model_ebics/model/results_0.0001_0.75_25.0_0.55_25.0"
sim_number = 0 #str(sys.argv[1])
make_pattern = str(sys.argv[1])
#log_file = "pattern_%i.log" % sim_number
#f = open(log_file,'w+')

#time_points = [ int(x) for x in sys.argv[2:] ]#convert 1 more more timepoints to int

##### Set up folders for image generation
p = os.path.join(folder,str(sim_number))
print("Loading files in %s") % p


###### Generate Patterns #####
ts = TimeSeries(p)
convergence_time = ts.convergence_time
pattern_folder = 'patterns_2D_matplotlib'
print "Convergence time is %i" % convergence_time
critical_points = [math.ceil(x*convergence_time) for x in [0.2,0.3,0.4,0.5,0.6]]

## Analyze 1 or more time points
for time_point in critical_points:
    ##Current cell from simulation
    tp = ts.get_time_point(time_point)
    print "Loading time point %i" % time_point
    """agents = tp.nodes()
    count_states(agents,time_point)
    af.save_2D_cell_pattern(p,tp,time_point,pattern="sim",rng=350)
    """

    #generate patterns
    #1250 Images
    if make_pattern == 'random' or make_pattern == 'all':
        for percent_rand in np.linspace(.2,.8,50):
            for rep in np.arange(0,10,1):
                r_agents = PG.Zero(tp)
                r_agents = PG.Random(percent=percent_rand,network=tp)
                #count_states(r_agents.nodes(),time_point)    
                 
                #save image
                pattern = "random_t%s_p%.02f_rep%i" % (str(time_point).zfill(3),percent_rand,rep)
                #af.save_2D_cell_pattern(p,tp,time_point,pattern=pattern_name,rng=350)
                save_path = os.path.join(pattern_folder,'pylab_RANDOM_2D_trans')
                af.draw_cells_matplotlib(save_path, r_agents,pattern=pattern)
        
    ###Outside IN
    if make_pattern == 'outsidein' or make_pattern == 'all':
	for percent_out in np.linspace(.2,.8,200):
	    OutIn_agents = PG.Zero(tp)
	    OutIn_agents = PG.OutsideIn(percent_radius=percent_out,network=tp)
	    #count_states(OutIn_agents.nodes(),time_point) #print progress to screen
	    
	    #save image
	    pattern = "outin_t%s_p%.04f" % (str(time_point).zfill(3),percent_out)
	    #af.save_2D_cell_pattern(p,tp,time_point,pattern=pattern,rng=350)
	    save_path = os.path.join(pattern_folder,'pylab_OUTSIDEIN_2D_trans')
	    af.draw_cells_matplotlib(save_path, OutIn_agents,pattern=pattern)
     
    ##insideout. 50*5=250.
    if make_pattern == 'insideout' or make_pattern == 'all': 
        steps = np.linspace(.2,.8,200)
        for percent_in in steps:
            inOut_agents = PG.Zero(tp)
            inOut_agents = PG.InsideOut(percent_radius=percent_in,network=tp)
            #count_states(inOut_agents.nodes(),time_point)
        
            #Save image
            pattern = "inout_t%s_p%.04f" % (str(time_point).zfill(3),percent_in)
            #af.save_2D_cell_pattern(p,tp,time_point,pattern=pattern,rng=350)
            save_path = os.path.join(pattern_folder,'pylab_INSIDEOUT_2D_trans')
            af.draw_cells_matplotlib(save_path, inOut_agents,pattern=pattern)
        

    #Globular
    if make_pattern == 'globular' or make_pattern == 'all': 
        for seeds in range(3,12):
             #seed size less than 4 not many patterns
             for seed_size in [5,10,20,30,50,100,200,300,400,500]:
                 for rep in np.arange(0,15,1):  #rep - replicate to capture random variability
                     globular_agents = PG.Zero(tp)
                     globular_agents = PG.Globular(seeds=seeds, count=seed_size, network=tp)
                     p_diff = count_states(globular_agents.nodes(),time_point)
                     
                     if p_diff >= 15 and p_diff <= 85:
                         pattern = "globular_p%.02f_se%s_sz%s_%i" % (p_diff,str(seeds),str(seed_size),rep)
                         #af.save_2D_cell_pattern(p,tp,time_point,pattern=pattern,rng=350)
                         save_path = os.path.join(pattern_folder,'pylab_GLOBULAR_2D_trans')
                         af.draw_cells_matplotlib(save_path, globular_agents,pattern=pattern)
                     
     
    #Snaked Patterns
    if make_pattern == 'snaked' or make_pattern == 'all':
        #less than 3 no visual patterns
        #min percent diff should be .2 or 20%. max .8 or 80%
        for seeds in range(3,10):
             #dont exceed 500. max recursion error.
            for seed_size in [20,30,40,50,60,100,500]:
                for rep in 'abcdefghij':  #rep = replicate to capture random pattern variability
                    snaked_agents = PG.Zero(tp)
                    snaked_agents = PG.Snaked(seeds=seeds, length=seed_size, network=tp)
                    p_diff = count_states(snaked_agents.nodes(),time_point)
                    
                    if p_diff >= 15 and p_diff <= 85:
                        pattern = "snaked_%.03f_se%s_sz%s_%s" % (p_diff,str(seeds),str(seed_size),rep)
                        save_path = os.path.join(pattern_folder,'pylab_SNAKED_2D_trans')
                        #af.save_2D_cell_pattern(p,tp,time_point,pattern=pattern,rng=350)
                        af.draw_cells_matplotlib(save_path, snaked_agents, pattern=pattern)

