################################################################################
# Name:   Analysis Functions
# Author: Douglas E. White
# Date:   11/19/2013
################################################################################

# Has functions for analyzing network time series
import simulationMath as simMath
# from visual import *
import time as t_m
#from PIL import ImageGrab, Image, ImageDraw, ImageFont
from PIL import Image, ImageDraw, ImageFont
import pyscreenshot as ImageGrab
import numpy as np
import networkx as nx
#from Colormaps import colormap
import subprocess
    
def ZeroPad(maxTime, time):
    """ Zero pads the current number
        Returns the current number as an string weith the correct number
        of 0s appended
    """
    #figure out the max number of diogits in the final time
    l = math.log(maxTime, 10)
    if(abs(l - math.ceil(l)) < 0.000001):
        l = math.ceil(l)
    d1 = int(math.floor(l))
    #figure out the number of diogits in the current time
    if(time == 0):
        d2 = 0
    else:
        l = math.log(time, 10)
        if(abs(l - math.ceil(l)) < 0.000001):
            l = math.ceil(l)
        d2 = int(math.floor(l))
    #now append 0's the fron of the current time proportional to the difference
    s = ""
    for i in range(0, d1-d2):
        s = s + "0"
    s = s + repr(time)
    return s

def GetCenter(network):
    #gets the center of a list of agents in a network
    agents = network.nodes()
    cent = [0,0,0]
    for i in range(0, len(agents)):
        cent = simMath.AddVec(cent, agents[i].location)
    #Normalize
    cent = simMath.ScaleVec(cent, 1.0/len(agents))
    #now return
    return cent   

def GetRadiusandCenter(network):
    #first get the center
    center = GetCenter(network)
    #gets the maximum radial size of the network
    agents = network.nodes()
    radius = -1
    for i in range(0, len(agents)):
        r = simMath.SubtractVec(center, agents[i].location)
        radius = max(simMath.Mag(r), radius)
    #now return
    return radius, center

def draw_cells_2D(network, thickness = 10, plane = 0):
    """ Filter a 3D cell set down to a 2D cell plane of thickness
        thickness*2. The plane to used is specified by the plane
        function as follows:
        0 - xy
        1 - xz
        2 - yz
        Returns - a network with the contained nodes
    """
    if(plane == 0):
        #takes a networkX object to use to draw with
        scene.center = GetCenter(network)
        #for each agent in the nodes draw a sphere
        agents = network.nodes()
        for agent in agents:
            #calculate the distance in z
            if(abs(agent.location[2] - scene.center[2]) < thickness):
                #then you can plot it
                if(agent.sim_type == "stemcell"):
                    if(agent.state == "U" or agent.state == "T"):
                        col = [0, 1.0, 1.0]
                        op = 0.25
                    if(agent.state == "D"):
                        col = color.blue
                        op = 0.5
                    if(agent.state == "A"):
                        col = color.red
                        op = 1.0    
    elif(plane == 1):
            #calculate the distance in y
            if(abs(agent.location[1] - scene.center[1]) < thickness):
                #then you can plot it
                if(agent.sim_type == "stemcell"):
                    if(agent.state == "U" or agent.state == "T"):
                        col = [0, 1.0, 1.0]
                        op = 0.25
                    if(agent.state == "D"):
                        col = color.blue
                        op = 0.5
                    if(agent.state == "A"):
                        col = color.red
                        op = 1.0 
    elif(plane == 3):
            #calculate the distance in x
            if(abs(agent.location[0] - scene.center[0]) < thickness):
                #then you can plot it
                if(agent.sim_type == "stemcell"):
                    if(agent.state == "U" or agent.state == "T"):
                        col = [0, 1.0, 1.0]
                        op = 0.25
                    if(agent.state == "D"):
                        col = color.blue
                        op = 0.5
                    if(agent.state == "A"):
                        col = color.red
                        op = 1.0 
    else:
        return None

def draw_cells(network):
    #takes a networkX object to use to draw with
    scene.center = GetCenter(network)
    print(scene.center)
    #for each agent in the nodes draw a sphere
    agents = network.nodes()
    print("Agents to draw: " + repr(len(agents)))
    for i in range(0, len(agents)):
        agent = agents[i]
        #Now color code based on the type
        if(agent.sim_type == "stemcell"):
            if(agent.state == "U" or agent.state == "T"):
                col = [0, 1.0, 1.0]
                op = 0.01
##            if(agent.state == "T"):
##                col = color.green
##                op = 1.0
            if(agent.state == "D"):
                col = color.blue
                op = 1.0
            if(agent.state == "A"):
                col = color.red
                op = 1.0

def save_2D_cell_images(save_path, data, rng = 250, scale_bar = 50):
    """ Draws cell images in 2D
        save_path - the path to save image at
        data - the data to load
        *optional* rng - the spatial range to draw in
        *optional* scale_bar  - the size of the scale bar to draw on the image
    """
    #define the bounding box to take images at
    bbox = [10,30,590,570]
    #save all of the images
    images = []
    #gte the itmes
    times = data.get_times()
    #load a new font
    my_font = ImageFont.truetype("arial.ttf", 14)
    #loop over these
    for i in range(0, len(times)):
        #make the drawing scene
        scene = display(x = 0, y = 0, width = 600, height = 600, autoscale = 0)
        scene.background = color.white
        scene.exit = False
        #also set the range
        scene.range = rng
        time = times[i]
        print(time)
        print("Loading...")
        #get Graph at the time point N
        tp = data.get_time_point(time)
        #Now filter the time point in 2D
        
        #Now render the cells
        print("Drawing...")
        draw_cells_2D(tp)
        #wait one seconds
        t_m.sleep(1)
        print("Save the image")
        im = ImageGrab.grab(bbox)
        #write the time in the upper left hand corner
        draw = ImageDraw.Draw(im)
        draw.text((10,10),
                  "Time : " + repr(time),
                  font = my_font,
                  fill = (0,0,0))
        #save a scale bar in the upper right hand corner
        #figure out the wdith
        width = (600 / (rng*2.))*scale_bar
        #set the text to draw
        text = repr(scale_bar) + " um"
        #get the size for drawing
        w, h = draw.textsize(text, font = my_font)
        width = max(width, w)
        #make the text
        draw.text((580 - 10 - width, 10),
                  text,
                  font = my_font,
                  fill = (0,0,0))
        #now draw the line at 10 + 1.1*h
        draw.line(((580 - 10 - width, 10 + 1.75*h),
                   (580 - 10, 10 + 1.75*h)),
                  fill = (0,0,0))
        #get the zero padded image save name to order the images correctly
        s = ZeroPad(times[-1], time)
        im.save(save_path + "agents_2D_" + s +".png", "PNG")

        #delete the scene
        scene.visible = False
        del scene
    #now make the movie
    #get the number of digits to use
    """
    s = s.split('.')
    s = len(s[0])
    subprocess.call(["C:\\users\\doug\\desktop\\CM3Dv4\\MakeMovie.bat",
             save_path +"agents_2D_" "%0" + repr(s) + "d.0.png",
             save_path +"agents_2D_"])
    """

def save_cell_images(save_path, data, rng = 250, scale_bar = 50):
    """ Draws cell images in 3D
        save_path - the path to save image at
        data - the data to load
        *optional* rng - the spatial range to draw in
        *optional* scale_bar  - the size of the scale bar to draw on the image
    """
    #deinfe the bounding box to take images at
    bbox = [10,30,590,570]
    #save all of the images
    images = []
    #gte the itmes
    times = data.get_times()
    #load a new font
    #my_font = ImageFont.truetype("arial.ttf", 14)
    my_font = ImageFont.load_default()
    #prelabel all of the nodes with a pram binding them to a sphere
    #now for each time get the network
    for i in range(0, len(times)):
        #make the drawing scene
        scene = display(x = 0, y = 0, width = 600, height = 600, autoscale = 0)
        scene.background = color.white
        scene.exit = False
        #get the time
        time = times[i]
        print(time)
        print("Loading...")
        #get the time point
        tp = data.get_time_point(time)
        #Now render the cells
        print("Drawing...")
        draw_cells(tp)
        #also set the range
        scene.range = rng
        #wait one seconds
        t_m.sleep(1)
        print("Save the image")
        im = ImageGrab.grab(bbox)
        #write the time in the upper left hand corner
        draw = ImageDraw.Draw(im)
        draw.text((10,10),
                  "Time : " + repr(time),
                  font = my_font,
                  fill = (0,0,0))
        #save a scale bar in the upper right hand corner
        #figure out the wdith
        width = (600 / (rng*2.))*scale_bar
        #set the text to draw
        text = repr(scale_bar) + " um"
        #get the size for drawing
        w, h = draw.textsize(text, font = my_font)
        width = max(width, w)
        #make the text
        draw.text((580 - 10 - width, 10),
                  text,
                  font = my_font,
                  fill = (0,0,0))
        #now draw the line at 10 + 1.1*h
        draw.line(((580 - 10 - width, 10 + 1.75*h),
                   (580 - 10, 10 + 1.75*h)),
                  fill = (0,0,0))
        #get the zero padded image save name to order the images correctly
        s = ZeroPad(times[-1], time)
        im.save(save_path + "agents_" + s + ".png", "PNG")

        #delete the scene
        scene.visible = False
        del scene
    #now make the movie
    #get the number of digits to use
    s = s.split('.')
    s = len(s[0])
    subprocess.call(["C:\\users\\doug\\desktop\\CM3Dv4\\MakeMovie.bat",
             save_path +"agents_" "%0" + repr(s) + "d.0.png",
             save_path +"agents_"])

