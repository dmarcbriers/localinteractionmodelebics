import argparse
import sys,os,errno
import re
import time
import random as r

from scipy.spatial.distance import euclidean
import networkx as nx

from simulationObjects import *
from Simulation import *


def make_sure_path_exists(path):
    """ Function for safely making output folders"""
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

start_time = time.time() #record runtime on any OS


if(__name__ == '__main__'):

    #Create argparse object
    parser = argparse.ArgumentParser(description=\
    "This is an agent-based model that captures spatial differentiaion patterns\n" + \
    "in aggregates of stem cells known as Embroid Bodies. See white et al Integrative Biol (2015).")
    #Set command line arguments
    parser.add_argument("sim_id",
                        help="There are several starting simulation structures numbered 0-99 to " +\
                             "capture biological variability. Choose any structure numbered 0-99.",
                        type=str)
    parser.add_argument('a',help='basal differentiaion probability. Range: .01,.005,.002',type=float)
    parser.add_argument('k1',help='tuning paramter for negative feedforward rule. Range: [0,1]',type=float)
    parser.add_argument('n1',help='Hill coefficient for negative feedforward rule. Range: 10,25,50',type=float)
    parser.add_argument('k2',help='Tuning parameter for positive feedback rule. Range: [0,1]',type=float)
    parser.add_argument('n2',help='Hill coefficient for positive feedback rule. Range: 10,25,50',type=float)
    parser.add_argument('time_end',help='Maximum number of time steps the model will run.(Default=200). Sim runs until time_end or all cells are differentiated.',
                        nargs='?',type=int,default=200)

    args = parser.parse_args()
    
    #Parameter synthesis defaults.
    ts = 0   #start_time
    te = args.time_end # 144(paper2013), 500(paper2015) time steps.
    dt = 1   #time_step
    sim_id = args.sim_id

    #output folder point to relative dir. This can also be absolute directory.
    output_folder = ["results",str(args.a),str(args.k1),str(args.n1),str(args.k2),str(args.n2)]
    sim_output_path = '_'.join(output_folder)+ os.sep
    struct_path =  os.path.join("1000", sim_id) ## take structural file from /sim/base/path/1000/sim_id/

    #Make sure path exists, create log file
    make_sure_path_exists(os.path.join(sim_output_path,sim_id))
    log_file = open( os.path.join(sim_output_path,sim_id,'simulation.log') ,"w")
    sys.stdout = log_file
     
    ### Do not modify code below for parameter synthesis. ###
    #########################################################################################################
    
    print "Starting simulaiton with params a-%s,k1-%s,n1-%s,k2-%s,n2-%s" \
           % (str(args.a),str(args.k1),str(args.n1),str(args.k2),str(args.n2))

    #Now make a new simulation
    sim = Simulation(sim_id,
                     sim_output_path,
                     ts,
                     te,
                     dt)
    p1 = args.a  #0.01 #basal random differentiation rate
    p2 = args.k1  #0.3 #normalized repsonse prameters, should range from 0 - 1. knf
    p3 = args.k2  #0.5 #DB normalized repsonsea prameters, should range from 0 - 1. kpf
    p4 = args.n2  #25 #coefficient controlling the stepness of the sigmoidal response function. n_p
    p5 = args.n1  #25 #coefficient controlling the stepness of the sigmoidal response function. n_n
        #knf = self._params[1]
        #kpf = self._params[2]
        #n_p = self._params[3]
        #n_n = self._params[4]

    #find the only file in the dir
    f = os.listdir(struct_path)
    
    ## Fix for mac since it puts other files in every directory.
    #clean_fileList = []
    #for file in f:
    #    if re.search('gpickle',file,re.I):
    #        clean_fileList.append(file)
    #f = clean_fileList
    #open the input structure file
    net = nx.read_gpickle(os.path.join(struct_path,f[0]))
    cent = (0,0,0)
    placed = False
    nodes = net.nodes()
    np.random.shuffle(nodes)
    id_map = dict()
    for i in range(len(nodes)):
##        if(i % 25 != 0):
        node = nodes[i]
        #it's a cell
        #randmoize the division set
        div_set = r.random()*19
        #make a new sim object out of this
        #make sure to randomize the division set time
        sim_obj = StemCell(node.location, node.radius, node.ID,
                           state = "U", division_set = div_set,
                           params = [p1, p2, p3, p4, p5])

        #keep track of the ID dict for the connection mapping
        id_map[node.ID] = sim_obj
        #add it to the sim
        sim.add(sim_obj)
    #also import the connections to use as well
    cons = net.edges()
    for i in range(0, len(cons)):
        con = cons[i]
        n1, n2 = con
        s1 = id_map[n1.ID]
        s2 = id_map[n2.ID]
        sim.network.add_edge(s1, s2)
    
    """Why do we need to recreate the edges list?? it never changed
    sim.network.add_edges_from(
       #[id_map[edge1.ID],id_map[edge2.ID] for edge1,edge2 in network.edges()]
      )
    """
    #Run the simulation
    sim.run()

    # Print runtime of script
    print 'Model took', time.time()-start_time, 'seconds.'


    log_file.close()
