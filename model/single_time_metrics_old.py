#python modules
import time as t_m
import subprocess
import os,sys
import math

#3rd party modules
from scipy import spatial
import networkx as nx

#local modules
#from PIL import ImageGrab, Image
#from visual import *
from Simulation import *
from simulationMath import *
from simulationObjects import *
import PatternGenerator as PG
import Analysis as af


def collide_kdtree(network):

    
    #Netwoks has new nodes from update_object_queue
    
    #Get locations from StemCell Class
    agents = network.nodes()
    agent_locations = np.zeros((len(agents), 3))
    for i in range(len(agents)):
        agent_locations[i] = agents[i].location
    t_agent_locations = tuple(map(tuple, agent_locations))
    
    #create kd tree. all object nodes are in network
    tree = spatial.cKDTree(t_agent_locations)
    
    r = [agent.radius for agent in agents]
    print "count: %i max: %.03f min:%.03f mean:%.03f std:%.03f" % (len(r),np.max(r), np.min(r),np.mean(r),np.std(r))
    
    
    #Must check all nodes since structural model may have moved some cells too far? 
    #num_new_objects = 300 #len(self.object_to_add)
    #new_agents = agents[-num_new_objects:]
    #new_agent_locations = t_agent_locations[-num_new_objects:]
    
    #Perform kdtree distance search to find all edges.
    max_distance = 13  #6.5*2
    #new_edges_indices = tree.query_ball_point(new_agent_locations,r=max_distance,p=2)
    all_edges_indices = tree.query_ball_tree(tree,r=max_distance,p=2)
    
    #Remove self edges by performing query on indices with distance of 0.
    #q1_d, self_indices = tree.query(_objects_to_add_locations,k=1,p=2,distance_upper_bound=0.1)
    min_distance = 0.1
    self_edges_indices = np.arange(0,len(agents),step=1)
    #self_indices = tree.query_ball_point(new_agent_locations,r=min_distance,p=2)
    #self_edges_indices = tree.query_ball_tree(tree,r=min_distance,p=2)
    #print self_indices[0:10]
    
    #Remove self edges. TODO: FLANN C implementation
    #perform set differnce instead of looping and finding eucl distance > 0
    neighbor_indices = np.array([np.setdiff1d(all_edges_indices[i],self_edges_indices[i]) for i in range(len(self_edges_indices))])
    #print 'The array', all_edges_indices[1],'became',neighbor_indices[1]
    
    #create tuples/edges and add to network
    updated_edges = []
    for i in range(len(agents)):
        agent = agents[i]
        agent_neighbors = np.array(neighbor_indices[i])
        edges_tuples = [(agent,agents[idx]) for idx in agent_neighbors]
        updated_edges.append(edges_tuples)
        #network.add_edges_from(edges_tuples)
    network.add_edges_from(edges_tuples)
    #Clean up.
    #self._objects_to_add = []
    #print dist

def collide(network):
    """ Handles the collision map generation 
    """
    self_objects = network.nodes()
    #first make a list of points for the delaunay to use
    points = np.zeros((len(self_objects), 3))
    index_to_object = dict()
    for i in range(0, len(self_objects)):
        #Now we can add these points to the list
        points[i] = self_objects[i].location
    #now perform the nearest neghbor assessment by building a delauny triangulation
    tri = spatial.Delaunay(points)
    #keep track of this as a network
    network = nx.Graph()
    #add all the simobjects
    network.add_nodes_from(self_objects)
    #get the data
    nbs = tri.vertices
    #iterate over all the nbs to cull the data for entry into the list
    for i in range(0, len(nbs)):
        #loop over all of the combination in this list
        ns = nbs[i]
        for a in range(0, len(ns)):
            for b in range(a+1, len(ns)):
                #these are the IDs
                ns[a]
                ns[b]
                network.add_edge(self_objects[ns[a]],
                                      self_objects[ns[b]])
    #now loop over all of these cells and cull the interaction lists
    edges = network.edges()
    #keep track of interactions checked
    count = 0
    for i in range(0, len(edges)):
        #get the edge in question
        edge = edges[i]
        #first find the distance of the edge
        obj1 = edge[0]
        obj2 = edge[1]
        #figure out if the interaction is ok 
        #gte the minimum interaction length
        l1 = obj1.get_max_interaction_length()
        l2 = obj2.get_max_interaction_length()
        #add these together to get the connection length
        interaction_length = l1 + l2
        #get the distance
        dist_vec = SubtractVec(obj2.location, obj1.location)
        #get the magnitude
        dist = Mag(dist_vec)
        #if it does not meet the criterion, remove it from the list
        if(dist > interaction_length):
            network.remove_edge(obj1, obj2)
            #increment the count
            count += 1
    print(repr(count) + " of " + repr(len(edges)) + " edges rejected")
    
def count_states(agents,time):
    
    total=len(agents)
    D=0
    U=0

    for agent in agents:
        if (agent.state == "D"):
            D +=1
        if(agent.state == "T"):
            D+=1
        if (agent.state == "U"):
            U +=1
    
    #Total number of cells
    tot = U + D

    # Percent Differentiated    
    if D > 0:
        percent_diff = float(D)/float(tot)*100
    else:
    	percent_diff = 0

    print ("Time\tCells\tPDiff\tU\tD\n%i\t%i\t%.03f\t%i\t%i") % (time,tot,percent_diff,U,D)


#path1 = "/home/dbriers/hyness/EBICS/clean_model/results"
#paths = [path1]

#Load Data
folder = 'Z:\Dropbox\repos\localinteractionmodelebics\model\results_0.001_0.3_25.0_0.5_25.0'
sim_number = '1' #str(sys.argv[1])
#time_points = [ int(x) for x in sys.argv[2:] ]#convert 1 more more timepoints to int

##### Set up folders for image generation
p = os.path.join(folder,sim_number)
image_path = os.path.join(p,"images",os.sep)
print("Loading files in %s") % p


#####


###### Generate Patterns #####
ts = TimeSeries(p)
convergence_time = ts.convergence_time
critical_points = [math.ceil(x*convergence_time) for x in [0.2,0.4,0.6]]

## Analyze 1 or more time points
for time_point in [25]:#[25,50,75,100,125]: #critical_points:
    ##Current cell from simulation
    test_network1 = ts.get_time_point(time_point)
    #test_network2 = test_network1.copy() #deepcopy
    #agents = tp.nodes()
     

    #Test printing images w matplotlib
    #save_path = os.path.join(folder,sim_number)
    #af.draw_cells_matplotlib(save_path, test_network1, time_point)


    """
    start_time = time.time()
    collide(test_network1)
    t1= time.time()-start_time
    print 'Delanauy method took',t1,'seconds'
    
    #Time for kkdtree.
    start_time = time.time()
    collide_kdtree(test_network2)
    t0 = time.time()-start_time
    print 'KD Tre method took',t0,'seconds'
    print 'KDtree is %.05f times faster' % (t1/t0)
    """
    
    
    """count_states(agents,time_point)
    af.save_2D_cell_pattern(p,agents,time_point,rng=350)

    """
    
    """
    #generate patterns
    #note. existing network needs to be zeroed out??
    r_agents = PG.Zero(tp)
    r_agents = PG.Random(percent=0.5,network=tp)
    count_states(r_agents.nodes(),time_point)    
    
    undiff_clusters = af.compute_clusters(network=tp,comparator=af.is_oct4_pos)
    diff_clusters = af.compute_clusters(network=tp,comparator=af.is_oct4_neg)
    
    print "There are %i oct4+clusters" % len(undiff_clusters)
    for c in undiff_clusters:
        print "There are %i nodes in this cluster." % len(c.nodes())
    
    print "There are %i oct4-clusters" % len(diff_clusters)
    for c in diff_clusters:
        print "There are %i nodes in this cluster." % len(c.nodes())
    """
    
    """
    ###Outside IN
    OutIn_agents = PG.Zero(tp)
    OutIn_agents = PG.OutsideIn(percent_radius=0.5,network=tp)
    count_states(OutIn_agents.nodes(),time_point)
    """
    ##insideout
    tp = test_network1
    inOut_agents = PG.Zero(tp)
    inOut_agents = PG.InsideOut(percent_radius=0.5,network=tp)
    count_states(inOut_agents.nodes(),time_point)
    #clusters = af.compute_clusters(network=tp,comparator=af.is_oct4_plus)
    #PG.
    print "There are %i clusters" % len(clusters)
