# This script makes a movie from an exsisting simulation and saves the resulting
# images on the background

import os, sys
#add scipy path
#sys.path.append("/Users/hyness/anaconda/lib/python2.7/site-packages/") #import scipy for Vidle

from Simulation import TimeSeries
import VisualizeAgents as af2
from PIL import Image
#import pyscreenshot as ImageGrab
import networkx as nx
#import time as t_m
import subprocess

p = "C:\\Users\\dbriers\\Documents\\BU\\Belta Lab\\White\\clean_model2\\clean_model\\results" #"/Users/hyness/Documents/Demarcus/clean_model/results" #path
num_sims = 2

#for example C:\\users\\doug\\sim1\\
#where sim1 is the folder containing simulations (which I generally label 1 - n)
#this script will make a movie of just the first simulation in this subfolder
#fell free to adapt this to suit your needs
print("Starting image caputuring")
for sim_number in [2]: #range(0, num_sims):
    if(os.path.isdir(p)):
        #get simulation folder
        sim_folder = p + os.sep + str(sim_number) + os.sep #trailing os.sep necessary
        
        #load the data
        ts = TimeSeries(sim_folder)

        #make a new directory
        try:
            #os.mkdir(p + os.sep + "movie")
            os.makedirs(p + os.sep + "images")
        except OSError:
            print("overwritting existing directory")
            
        #save the data images or pssibly movies
        #af.save_cell_images(p + os.sep + "images" + os.sep, ts, rng = 350)
        #af.save_2D_cell_images(p + os.sep + "images" + os.sep, ts, rng = 350)
        af2.save_3D_cell_images(sim_folder+"images", ts, rng = 350)
print("saving simulation image")

