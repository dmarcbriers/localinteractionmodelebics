################################################################################
# Name:   Analysis Functions
# Author: Douglas E. White
# Date:   11/19/2013
################################################################################

# Has functions for analyzing network time series
from __future__ import division
import subprocess
import sys,os,errno
import time as t_m

#import visual as vis
#from PIL import ImageGrab, Image, ImageDraw, ImageFont
import numpy as np
import networkx as nx
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as py
#import pylab as p
#py.ion()

#from vis.colormaps import vis.colormap
import simulationMath as simMath

def compute_clusters(network, comparator, lower_lim = 1):
    """ Evaluates clusters which may be present based on the comparison
        function provided. Returns a list of these networks as networkX
        graphs.

        network - networkx object. NOT a gpickle location. Open file first.
        comparator - a function that checks the state of a Simulation Object. see 
    """
    networks = []
    #first get all the nodes in the network
    nodes = network.nodes()
    #copy the network
    g = network.copy()
    nodes = g.nodes()
    print("Examining " + repr(len(nodes)) + " nodes")
    for i in range(0, len(nodes)):
        #tag all the nodes with the examined tag
        nodes[i].examined = False
        nodes[i].in_list = False
    #save the networks
    clusters = []
    #keep track fo the nodes to add
    nodes_to_check = []
    #now iterate
    for i in range(0, len(nodes)):
        #if the node has not been examined
        if(not nodes[i].examined):
            #add the first node to the node to check
            nodes_to_check = [nodes[i]]
            #now iterate over the list
            j = 0
            while(j < len(nodes_to_check)):
                #check to see if this is examined already
                if(not nodes_to_check[j].examined):
                    #get the neighbors and add these to the list
                    nbs = g.neighbors(nodes_to_check[j])
                    nodes_to_check[j].examined = True
                    #now loop over all fo the neighbros
                    for k in range(0, len(nbs)):
                        #check to see it is in the examine list
                        if(not nbs[k].in_list):
                            #check to see if it fits the criterion
                            if(comparator(nbs[k])):
                                #then add it to the list
                                nodes_to_check.append(nbs[k])
                                nbs[k].in_list = True
                #increment j
                j += 1
            #now we have all of the nodes in the nodes to check list
            #make a new graph
            if(len(nodes_to_check) > lower_lim):
                n = nx.Graph()
                #add these nodes to the graph
                n.add_nodes_from(nodes_to_check)
                #also add all the connections
                for i in range(0, len(nodes_to_check)):
                    #get the neighbors of n
                    node = nodes_to_check[i]
                    nbs = network.neighbors(node)
                    #add them all to the network
                    for other in nbs:
                        n.add_edge(node, other)
                #add this to the list
                clusters.append(n)
    #now return the clusters
    return clusters

############ Define comparators/metrics for network analysis
def is_oct4_plus(stem_cell_agent):
    """
    Oct4 is a protein that indicates a pluripotent cell, so
    this checks for undifferentiated cells (state=U or T).
    """
    if (stem_cell_agent.state == "U" or stem_cell_agent.state == "T"):
        return 1
    else:
        return 0


def count_states(agents,time):
    """ Print some basic metrics of a cell at a single time point
    total cells,percent differentiated,# in U state,# in D state.
    """
    tot=len(agents)
    D=0
    U=0

    for agent in agents:
        if (agent.state == "D"):
            D +=1
        elif (agent.state == "U" or agent.state == "T"):
            U +=1

    # Percent Differentiated    
    if D > 0:
        percent_diff = float(D)/float(tot)*100
    else:
        percent_diff = 0.0

    #print time image_name total_cells pdiff Udiff Difff 
    cell_info = "Time\tCells\tPDiff\tU\tD\n%s\t%i\t%.03f\t%i\t%i" % (time,tot,percent_diff,U,D) 
    print (cell_info) 
    
    #Just print info and no
    cell_statistics = "Time:%s,Cells:%i,Perc. Diff:%.03f,Undiff:%i,Diff:%i" % (time,tot,percent_diff,U,D)
    return cell_statistics


#########################################3


def make_sure_path_exists(path):
    """ Function for safely making a folders if it doesnt exist"""
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
    
def ZeroPad(maxTime, time):
    """ Zero pads the current number
        Returns the current number as an string weith the correct number
        of 0s appended
    """
    #figure out the max number of diogits in the final time
    l = vis.math.log(maxTime, 10)
    if(abs(l - vis.math.ceil(l)) < 0.000001):
        l = vis.math.ceil(l)
    d1 = int(vis.math.floor(l))
    #figure out the number of diogits in the current time
    if(time == 0):
        d2 = 0
    else:
        l = vis.math.log(time, 10)
        if(abs(l - vis.math.ceil(l)) < 0.000001):
            l = vis.math.ceil(l)
        d2 = int(vis.math.floor(l))
    #now append 0's the fron of the current time proportional to the difference
    s = ""
    for i in range(0, d1-d2):
        s = s + "0"
    s = s + repr(time)
    return s

def GetCenter(network):
    #gets the center of a list of agents in a network
    agents = network.nodes()
    cent = [0,0,0]
    for i in range(0, len(agents)):
        cent = simMath.AddVec(cent, agents[i].location)
    #Normalize
    cent = simMath.ScaleVec(cent, 1.0/len(agents))
    #now return
    return cent   

def GetRadiusandCenter(network):
    #first get the center
    center = GetCenter(network)
    #gets the maximum radial size of the network
    agents = network.nodes()
    radius = -1
    for i in range(0, len(agents)):
        r = simMath.SubtractVec(center, agents[i].location)
        radius = max(simMath.Mag(r), radius)
    #now return
    return radius, center

def draw_cells_2D(scene,network,thickness = 10, plane = 0, pattern="sim"):
    """ Filter a 3D cell set down to a 2D cell plane of thickness
        thickness*2. The plane to used is specified by the plane
        function as follows:
        0 - xy
        1 - xz
        2 - yz
        Returns - a network with the contained nodes
    """
    #NOTE: using colors black/red for image based pattern recognition
    if(plane == 0):
        #takes a networkX object to use to draw with
        scene.center = GetCenter(network)
        #for each agent in the nodes draw a sphere
        agents = network.nodes()
        for agent in agents:
            #calculate the distance in z
            if(abs(agent.location[2] - scene.center[2]) < thickness):
                #then you can plot it
                if(agent.sim_type == "stemcell"):
                    if(agent.state == "U" or agent.state == "T"):
                        col = vis.color.red #[0, 1.0, 1.0] #vis.color.cyan
                        op = 0.25
                    elif(agent.state == "D"):
                        col = vis.color.black #vis.color.blue
                        op = 0.5
                    elif(agent.state == "A"):
                        col = vis.color.red
                        op = 1.0 
                vis.sphere(pos = agent.location, radius = agent.radius, color = col, opacity = op)   
    elif(plane == 1):
        #takes a networkX object to use to draw with
        scene.center = GetCenter(network)
        #for each agent in the nodes draw a sphere
        agents = network.nodes()
        for agent in agents:
            #calculate the distance in y
            if(abs(agent.location[1] - scene.center[1]) < thickness):
                #then you can plot it
                if(agent.sim_type == "stemcell"):
                    if(agent.state == "U" or agent.state == "T"):
                        col = vis.color.red #[0, 1.0, 1.0]
                        op = 0.25
                    elif(agent.state == "D"):
                        col = vis.color.black  #vis.color.blue
                        op = 0.5
                    elif(agent.state == "A"):
                        col = vis.color.red
                        op = 1.0 
                vis.sphere(pos = agent.location, radius = agent.radius, color = col)
    elif(plane == 3):
            #calculate the distance in x
            if(abs(agent.location[0] - scene.center[0]) < thickness):
                #then you can plot it
                if(agent.sim_type == "stemcell"):
                    if(agent.state == "U" or agent.state == "T"):
                        col = vis.color.red  #[0, 1.0, 1.0]
                        op = 0.25
                    elif(agent.state == "D"):
                        col = vis.color.black #vis.color.blue
                        op = 0.5
                    elif(agent.state == "A"):
                        col = vis.color.red
                        op = 1.0 
                vis.sphere(pos = agent.location, radius = agent.radius, color = col,opacity=op)
    else:
        return None

def draw_cells(scene,network):
    #takes a networkX object to use to draw with
    scene.center = GetCenter(network)
    print("Graph center is at " + repr(scene.center))
    #for each agent in the nodes draw a sphere
    agents = network.nodes()
    
    print("Agents to draw: " + repr(len(agents)))
    for i in range(0, len(agents)):
        agent = agents[i]
        #Now vis.color code based on the type
        if(agent.sim_type == "stemcell"):
            if(agent.state == "U" or agent.state == "T"):
                col = [0, 1.0, 1.0]
                op = 0.1#0.01
##            if(agent.state == "T"):
##                col = vis.color.green
##                op = 1.0
            if(agent.state == "D"):
                col = vis.color.blue
                op = 0.7#1.0
            if(agent.state == "A"):
                col = vis.color.red
                op = 0.7#1.0
        vis.sphere(pos = agent.location, radius = agent.radius, color = col,opacity=op)
    
    
def save_2D_cell_images(save_path, data, rng = 250, scale_bar = 50):
    """ Draws cell images in 2D
        save_path - the path to save image at
        data - the data to load
        *optional* rng - the spatial range to draw in
        *optional* scale_bar  - the size of the scale bar to draw on the image
    """
    #deinfe the bounding box to take images at
    bbox = [10,30,590,570] #[0,0,1366,768] #
    #save all of the images
    images = []
    #gte the itmes
    times = data.get_times()
    #load a new font
    my_font = ImageFont.truetype("arial.ttf", 14)
    
    #loop over these
    for i in range(0, len(times)):
        #make the drawing scene
        scene = vis.display(x = 0, y = 0, width = 600, height = 600, autoscale = 0)
        scene.background = vis.color.white
        scene.visible = True
        #scene.exit = False
        #also set the range
        scene.range = rng
        time = times[i]
        print(time)
        print("Loading...")
        #get the time point
        tp = data.get_time_point(time)
        #Now filter the time point in 2D
        
        #Now render the cells
        print("Drawing...")
        draw_cells_2D(scene,tp,plane = 0,thickness = 20)
        #wait one seconds
        t_m.sleep(1)
        print("Save the image")
        im = ImageGrab.grab(bbox)
        
        ## TIME
        #write the time in the upper left hand corner
        draw = ImageDraw.Draw(im)
        """draw.text((10,10),
                  "Time : " + repr(time),
                  font = my_font,
                  fill = (0,0,0))
        """
        ## SCALE BAR
        #save a scale bar in the upper right hand corner
        #figure out the wdith
        width = (600 / (rng*2.))*scale_bar
        #set the text to draw
        text = repr(scale_bar) + " um"
        #get the size for drawing
        w, h = draw.textsize(text, font = my_font)
        width = max(width, w)
        #make the text
        draw.text((580 - 10 - width, 10),
                  text,
                  font = my_font,
                  fill = (0,0,0))
        #now draw the line at 10 + 1.1*h
        draw.line(((580 - 10 - width, 10 + 1.75*h),
                   (580 - 10, 10 + 1.75*h)),
                  fill = (0,0,0))
        ## INCLUDE CELL STATISITCS IN NAME/IMAGE
        cell_stats = count_states(tp.nodes(),repr(time))
        print cell_stats
        w, h = draw.textsize(cell_stats, font = my_font)
        x_coord =  10 #600/2 - w/2 ##Center the text
        y_coord = 10
        draw.text((x_coord, y_coord),
              cell_stats,
              font = my_font,
              fill = (0,0,0))
        

        #get the zero padded image save name to order the images correctly
        s = ZeroPad(times[-1], time)
        im.save(save_path + "agents_2D_" + s +".png", "PNG")

        #delete the scene
        scene.visible = False
        del scene
    #now make the movie
    #get the number of digits to use
    s = s.split('.')
    s = len(s[0])
    #subprocess.call(["C:\\Users\\dbriers\\Documents\\BU\\Belta Lab\\White\\clean_model2\\clean_model\\MakeMovie.bat",
             #save_path +"agents_2D_" "%0" + repr(s) + "d.0.png",
             #save_path +"agents_2D_"])
    
def save_2D_cell_pattern(save_path, network, time, rng = 250, scale_bar = 50, pattern="sim"):
    """ Draws cell images in 2D
        save_path - the path to save image at
        nework - networkX object
        *optional* rng - the spatial range to draw in
        *optional* scale_bar  - the size of the scale bar to draw on the image
    """
    make_sure_path_exists(save_path)
    
    #deinfe the bounding box to take images at
    bbox = [10,30,590,570] #[0,0,1366,768] #
    #save all of the images
    images = []
    #load a new font
    my_font = ImageFont.truetype("arial.ttf", 14)

    scene = vis.display(x = 0, y = 0, width = 600, height = 600, autoscale = 0)
    scene.background = vis.color.white
    scene.visible = True
        #scene.exit = False
        #also set the range
    scene.range = rng
    print(time)
    print("Loading...")
    
    #Now filter the time point in 2D
        
    #Now render the cells
    print("Drawing...")
    draw_cells_2D(scene,network,pattern=pattern,plane = 0,thickness = 20)
    #wait one seconds
    t_m.sleep(1)
    print("Save the image")
    im = ImageGrab.grab(bbox)
    #write the time in the upper left hand corner
    draw = ImageDraw.Draw(im)
    #draw.text((10,10),
    #          "Time : " + repr(time),
    #          font = my_font,
    #          fill = (0,0,0))
    #save a scale bar in the upper right hand corner
    #figure out the wdith
    width = (600 / (rng*2.))*scale_bar
    #set the text to draw
    text = repr(scale_bar) + " um"
    #get the size for drawing
    w, h = draw.textsize(text, font = my_font)
    width = max(width, w)
    #make the text
    draw.text((580 - 10 - width, 10),
              text,
              font = my_font,
              fill = (0,0,0))
    #now draw the line at 10 + 1.1*h
    draw.line(((580 - 10 - width, 10 + 1.75*h),
               (580 - 10, 10 + 1.75*h)),
                fill = (0,0,0))
                
    ## INCLUDE CELL STATISITCS IN NAME/IMAGE
    cell_stats = count_states(network.nodes(),repr(time))
    print cell_stats
    w, h = draw.textsize(cell_stats, font = my_font)
    x_coord =  10 #600/2 - w/2 ##Center the text
    y_coord = 10
    draw.text((x_coord, y_coord),
              cell_stats,
              font = my_font,
              fill = (0,0,0))
        
    #get the zero padded image save name to order the images correctly
    image_name = "agents_2D_%s_%s.png" % (str(time).zfill(3),pattern)
    im.save(os.path.join(save_path,image_name), "PNG")
    #s = ZeroPad(170, time)
    #im.save(save_path + "agents_2D_" + s +".png", "PNG")

    #delete the scene
    scene.visible = False
    del scene
    

def save_cell_images(save_path, data,rng = 250, scale_bar = 50):
    """ Draws cell images in 3D
        save_path - the path to save image at
        data - the data to load
        *optional* rng - the spatial range to draw in
        *optional* scale_bar  - the size of the scale bar to draw on the image
    """
    #deinfe the bounding box to take images at
    bbox = [10,30,590,570]
    #save all of the images
    images = []
    #gte the itmes
    times = data.get_times()
    #load a new font
    my_font = ImageFont.truetype("arial.ttf", 14)
    
    #prelabel all of the nodes with a pram binding them to a sphere
    #now for each time get the network
    for i in range(0, len(times)):
        #make the drawing scene
        scene = vis.display(x = 0, y = 0, width = 600, height = 600, autoscale = 0)
        scene.background = vis.color.white
        scene.exit = False
        #get the time
        time = times[i]
        print(time)
        print("Loading...")
        #get the time point
        tp = data.get_time_point(time)
        #Now render the cells
        print("Drawing...")
        draw_cells(scene,tp)
        #also set the range
        scene.range = rng
        #wait one seconds
        t_m.sleep(2)
        print("Save the image")
        im = ImageGrab.grab(bbox)
        #write the time in the upper left hand corner
        draw = ImageDraw.Draw(im)
        #draw.text((10,10),
        #          "Time : " + repr(time),
        #          font = my_font,
        #          fill = (0,0,0))
        #save a scale bar in the upper right hand corner
        #figure out the wdith
        width = (600 / (rng*2.))*scale_bar
        #set the text to draw
        text = repr(scale_bar) + " um"
        #get the size for drawing
        w, h = draw.textsize(text, font = my_font)
        width = max(width, w)
        #make the text
        draw.text((580 - 10 - width, 10),
                  text,
                  font = my_font,
                  fill = (0,0,0))
        #now draw the line at 10 + 1.1*h
        draw.line(((580 - 10 - width, 10 + 1.75*h),
                   (580 - 10, 10 + 1.75*h)),
                  fill = (0,0,0))
        ## INCLUDE CELL STATISITCS IN NAME/IMAGE
        cell_stats = count_states(tp.nodes(),repr(time))
        print cell_stats
        w, h = draw.textsize(cell_stats, font = my_font)
        x_coord =  10 #600/2 - w/2 ##Center the text
        y_coord = 10
        draw.text((x_coord, y_coord),
              cell_stats,
              font = my_font,
              fill = (0,0,0))
        
        #get the zero padded image save name to order the images correctly
        s = ZeroPad(times[-1], time)
        im.save(save_path + "agents_" + s +".png", "PNG")
        #delete the scene
        scene.visible = False
        del scene
    #now make the movie
    #get the number of digits to use
    s = s.split('.')
    s = len(s[0])
    subprocess.call(["C:\\Users\\dbriers\\Documents\\BU\\Belta Lab\\White\\clean_model2\\clean_model\\MakeMovie.bat",
             save_path +"agents_" "%0" + repr(s) + "d.0.png",
             save_path +"agents_"])

def draw_cells_matplotlib(save_path, network, pattern,thickness = 20,plane=0):
    #NOTE: using colors black/red for image based pattern recognition
    #log_file = open("simlog.log","w")
    #sys.stdout = log_file
    
    visible_cells_location = []
    visible_cells_radius = []
    visible_cells_color = []
    visible_cells_opacity = []
    
    if(plane == 0):
        #takes a networkX object to use to draw with
        center = GetCenter(network)
        #for each agent in the nodes draw a sphere
        agents = network.nodes()
        for agent in agents:
            #calculate the distance in z
            if(abs(agent.location[2] - center[2]) < thickness):
            #if(abs(agent.location[2] - center[2]) < thickness and center[2] > agent.location[2]):
                #then you can plot it
                if(agent.sim_type == "stemcell"):
                    if(agent.state == "U" or agent.state == "T"):
                        col = (1,0,0,.25)
                        op = 1.0 #0.25  #1/6.
                    elif(agent.state == "D"):
                        col = (0,0,0,0.5)
                        op = 1.0 #0.5  #2/6.
                    elif(agent.state == "A"):
                        col = vis.color.red
                        op = 1.0 
                #Save all properties in arrays
                visible_cells_location.append(agent.location)
                visible_cells_radius.append(agent.radius)
                visible_cells_color.append(col)
                #visible_cells_opacity.append(op)
    
    #sort by z-index
    visible_cells_location = np.array(visible_cells_location)
    index = visible_cells_location[:,2].argsort()
    visible_cells_location = visible_cells_location[index]
    
    visible_cells_radius = np.array([2*r*r for r in visible_cells_radius])
    visible_cells_radius = visible_cells_radius[index] 
    
    visible_cells_color = np.array(visible_cells_color)[index]
    
    #Plot using matplotlib
    save_path = os.path.join(save_path,pattern)+'.png'
    axis = py.figure()
    py.xlim((-350,350))
    py.ylim((-350,350))
    py.axes().set_aspect('equal', 'datalim')
    py.axis("off")

    py.scatter(visible_cells_location[:,0],
               visible_cells_location[:,1],
               s=visible_cells_radius,
               c=visible_cells_color,
               edgecolors='none'
              )
    
    py.savefig(save_path, bbox_inches='tight')
    py.close("all") 
    print "Plotting %i points" % len(visible_cells_radius)
    #log_file.close()
