## To use this script you need to install python-vtk and mayavi
#TODO-Demarcus: Added dividing cell as acceptable cell type

import os
#import simulationMath as simMath
import numpy
from mayavi.mlab import *
#from enthought.mayavi.modules.text import Text



def save_3D_cell_images(save_path,ts,rng):
#save_path = "/Users/hyness/Documents/Demarcus/stem_cell_model2/1000/0/images/"
#rng = 250

    ## Handle Input data and directories
    if not save_path.endswith(os.sep):
            save_path += os.sep
            
    data = ts
    times = data.get_times()
    
    #for t in range(0,10):
    for t in range(0, len(times)):
        time = times[t]
        tp = data.get_time_point(time)

        #Now render the cells
        #print("Agents to draw: " + repr(len(tp.nodes())))
        print("Drawing %i out of %i images.") % (t+1,len(times))
        draw_3D_cells(tp,save_path,t)


def draw_3D_cells(network,save_path,t):
    agents = network.nodes() ##or 
    
    ## Data objects
    loc = [] #save all locations in array
    color = [] # save colors in array
    
    #color mapping from Mayavi
    green = 0.55
    blue = 0.0
    cyan = 0.37
    red = 1.0
    
    for i in range(0, len(agents)):
        agent=agents[i]
        #print("Simulation type: "+agent.sim_type)
        #
        if(agent.sim_type == "stemcell" or agent.sim_type == "dividingcell"):
            if(agent.state == "U" or agent.state == "T"):
                col = cyan	
                op = 0.25
            if(agent.state == "D"):
                col = blue
                op = 0.5
            if(agent.state == "A"):
                col = red
                op = 1.0 
        loc.append(agent.location) #add xzy coordinates
        color.append(col)
    
    
    #save coordinates in split array for mayavi
    nplocations = numpy.array(loc)
    x = nplocations[:,0]
    y = nplocations[:,1]
    z = nplocations[:,2]
    
    ##Create mayavi figure to display image
    figure(1, bgcolor=(0, 0, 0))
    clf()
    
    
    #Use mayavi to plot points in 3D
    # colorMap is a color scheme.
    # vmin/vmax are binary representations(0 or 1) of differentiated or undifferentiated cells.
    # opacity lets us see interior cells
    image_3d = points3d(x,y,z,color,colormap='jet',vmin=0,vmax=1) #mapping of scalar numbers to some color. Can be customized.
    image_3d.glyph.color_mode = 'color_by_scalar'
    image_3d.glyph.scale_mode = 'data_scaling_off'
    options.offscreen = True
    
    #time_text = "Time: %i" % i
    #text(0.1, 0.9, time_text, color=(1,1,0))
    #my_text = Text()
    #my_text.text = time_text
    #script.add_module(t)
    #t.actor.scaled_text = False
    #my_text.actor.text_property.font_size = 34
    #fg_color = (1, 1, 1)
    #my_text.actor.text_property.color = fg_color
    # have to find out text width to center it. Tricky, but works fine. Thanks to Prabhu.
    #t.width = 1.0*t.actor.mapper.get_width(t.scene.renderer)/t.scene.renderer.size[0]
    #height = 1.0*t.actor.mapper.get_height(t.scene.renderer)/t.scene.renderer.size[1]
    #my_text.x_position = 0.1 #0.5-t.width/2
    #my_text.y_position = 0.9 #1-height
    
    #Save image to sim directory
    image_path = "agents_3D_%03d.png" % t
    image_path = os.path.join(save_path,image_path)
    savefig(image_path)
    
    nodes_D = color.count(blue)
    nodes_U = color.count(cyan)
    print("U clusters = %s and D clusters = %s") % (nodes_U,nodes_D)

