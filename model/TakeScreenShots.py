# This script makes a movie from an exsisting simulation and saves the resulting
# images on the background

import os, sys
from Simulation import TimeSeries
import Analysis as af
from PIL import ImageGrab, Image
import networkx as nx
from visual import *
import time as t_m
import subprocess

#Keep track of runtime
start_time = t_m.time() #record runtime on any OS


#path1 = "C:\\Users\\dbriers\\Documents\\BU\\Belta Lab\\White\\clean_model2\\clean_model\\results"
path1 = "C:\\Users\\dbriers\\Documents\\BU\\Belta Lab\\White\\clean_model2\\clean_model\\results_0.0001_0.75_25.0_0.55_25.0_sim0"
paths = [path1]
#for example C:\\users\\doug\\sim1\\
#where sim1 is the folder containing simulations (which I generally label 1 - n)
#this script will make a movie of just the first simulation in this subfolder
#fell free to adapt this to suit your needs
for i in range(0, len(paths)):
    p = paths[i]
    print(p)
    if(os.path.isdir(p)):
        p += os.sep + "0"
        #load the data
        print(p)
        ts = TimeSeries(p)

        #make a new directory
        try:
            #os.mkdir(p + "\\movie images\\")
            os.mkdir(p + os.sep + "images")
        except OSError:
            print("overwritting exssting directory")
            
        #save the data images
        image_path = os.path.join(p,"images" + os.sep)
        af.save_cell_images(image_path, ts, rng = 350)
        #af.save_2D_cell_images(image_path, ts, rng = 350)

## Print runtime
print 'It took', t_m.time()-start_time, 'seconds.'
