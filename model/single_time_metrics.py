#python modules
import time as t_m
import subprocess
import os,sys
import math

#3rd party modules
from scipy import spatial
import networkx as nx

#local modules
#from PIL import ImageGrab, Image
#from visual import *
from Simulation import *
from simulationMath import *
from simulationObjects import *
import PatternGenerator as PG
import AnalysisUpdated as af


    
def count_states(agents,time):
    
    total=len(agents)
    D=0
    U=0

    for agent in agents:
        if (agent.state == "D"):
            D +=1
        if(agent.state == "T"):
            D+=1
        if (agent.state == "U"):
            U +=1
    
    #Total number of cells
    tot = U + D

    # Percent Differentiated    
    if D > 0:
        percent_diff = float(D)/float(tot)*100
    else:
    	percent_diff = 0

    print ("Time\tCells\tPDiff\tU\tD\n%i\t%i\t%.03f\t%i\t%i") % (time,tot,percent_diff,U,D)


#path1 = "/home/dbriers/hyness/EBICS/clean_model/results"
#paths = [path1]

#Load Data
folder = 'Z:\Dropbox\\repos\localinteractionmodelebics\model\\results_0.001_0.3_25.0_0.5_25.0'
sim_number = '1' #str(sys.argv[1])
#time_points = [ int(x) for x in sys.argv[2:] ]#convert 1 more more timepoints to int

##### Set up folders for image generation
p = os.path.join(folder,sim_number)
#image_path = os.path.join(p,"images",os.sep)
print("Loading files in %s") % p


#####


###### Generate Patterns #####
ts = TimeSeries(p)
convergence_time = ts.convergence_time
critical_points = [math.ceil(x*convergence_time) for x in [0.2,0.4,0.6]]

## Analyze 1 or more time points
for time_point in [1]:#[25,50,75,100,125]: #critical_points:
    ##Current cell from simulation
    test_network1 = ts.get_time_point(time_point)

    ## Erase node states, and create outside-in pattern as a test.
    blank_network = PG.Zero(test_network1)
    inOut_agents = PG.InsideOut(percent_radius=0.5,network=blank_network)
    
    #Calculate global differentiated and undifferentiated cells
    # TODO: move this function to the analysis.py file
    count_states(inOut_agents.nodes(),time_point)
    
    #Locate the oct4positive cells
    oct4_neg_clusters = af.compute_clusters2(network=inOut_agents,comparator=af.is_oct4_neg, lower_lim=2)
    oct4_pos_clusters = af.compute_clusters2(network=inOut_agents,comparator=af.is_oct4_neg, lower_lim=2)

    ### Evaluate State of Clusters
    print "There are %i Oct4 negative clusters" % len(oct4_neg_clusters)
    for subCluster in oct4_neg_clusters:
        count_states(subCluster.nodes(),time_point)         

    #count_states(agents,time_point)
    #time_point=1
    #test_network2 = ts.get_time_point(time_point)
    #blank_network = PG.Zero(test_network2)
    #OutIn_agents = PG.OutsideIn(percent_radius=0.5,network=blank_network)
    #af.save_2D_cell_pattern(p,inOut_agents,time_point,rng=350)
    
    ##############################################
    # Plots to show Natalia that inversion does not happen from multi-layer slices.
    #af.draw_cells_matplotlib(save_path=p, network=inOut_agents, pattern="OutsideColored-1Kcells",thickness = 100,plane=0)
    #af.draw_cells_matplotlib(save_path=p, network=inOut_agents, pattern="OutsideColored-500cells",thickness = 20,plane=0)

        
    """
    #generate patterns
    #note. existing network needs to be zeroed out??
    r_agents = PG.Zero(tp)
    r_agents = PG.Random(percent=0.5,network=tp)
    count_states(r_agents.nodes(),time_point)    
    
    undiff_clusters = af.compute_clusters(network=tp,comparator=af.is_oct4_pos)
    diff_clusters = af.compute_clusters(network=tp,comparator=af.is_oct4_neg)
    
    """
    
    """
    ###Outside IN
    OutIn_agents = PG.Zero(tp)
    OutIn_agents = PG.OutsideIn(percent_radius=0.5,network=tp)
    count_states(OutIn_agents.nodes(),time_point)
    """

