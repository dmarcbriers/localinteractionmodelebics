###### This guide will explain how to run the model.
##### Background info:

See white et al 2013 for motivation of spatial patterns
See white et al 2015 integrative biology for descriptio of current model

The model consists of two parts.
1. Probablistic model stored as a networkX graph network.
2. A visualizaiton using vpython, for visual pattern recognition.


#### REQUIRED SOFTWARE
## Required for Simulation
scipy
numpy
networkx
## Required for visualization
visual - for making 3D images.
matplotlib (optional)
PIL - python imaging library


#####
### Running the probablistic model of local cell-cell interactons:
#####
    #1A. Single simulations
### ModelSteup.py is the file you need to run. Here are the parameters:

'python ModelSetup.py $SIM_ID a k1 n1 k2 n2'

  $SIM_ID - write a bash script to pass the number 1-99 to the model. You can run the model
	multiple time with the same set of parameters.
  a - random differentiation [.01, .001]
  k1 - normalization param for negative feedback. ranges from 0-1. [0.1, .5, .9]
  n1 - hill coefficient for negative feedback. Try 1-100? [10, 50]
  k2 - normalization param for positive feedback. ranges from 0-1 [.1, .5, .9]
  n2 - hill coefficient for posititve feedback. Try 1-100? [10, 50]


   #1B. Multiple simulaitons.
   To run multiple simulations, modify the bash script 'run_parallel_simulations.sh'. Make
   sure you have the gnu utility 'parallel' installed. This runs REPLICATES of a simulation.


   If you are using a cluster you could also launch a job array to queue simulations.

### Default Parameters are given from Doug Whites code. He recommended we use these parameters.
python ModelSetup.py $SIM_ID .01 .3 25 .5 25


#### NEW PARAMETER RANGES TO GUARENTEE Transition from undifferentiated, to patterned differentiated,
#    to completely differentiated forever.
a - [.001, .01] recommended: .001,.005, or .01
k1 - [0,1] recommended: 0.1,0.3,0.6,0.9
n1 - [1,100] recommended: 25(white 2013) 10,50(White 2015)
k2 - [0,1] recommended: range of [.7,1]. but white explored [0.1,0.3,0.6,0.9]
n2 - [1,100] recomended: 25(White 2013) 10,50(White 2015)

